1. Clone o projeto
2. Instale o node (https://nodejs.org/en/download/)
3. Instale o typescript (npm i typescript -g)
4. Instale o mongodb (https://www.mongodb.com/download-center/community)
5. Suba o serviço do MongoDb
4. Execute "npm install"
5. Execute "tsc"
6. Execute "node dist/main.js"


Pronto, a api estará funcionando.
Criei uma rota para popular a base com os dados do arquivo "pdvs.json"...
POST http://localhost:3000/pdvs/seed

Observações Gerais:
 - tive um problema no último teste que fiz, por esse motivo coloquei o json no import (sei que não é o melhor modo de fazer rsrs)
 - usei o operador $near do mongodb, imagino que o ideal fosse usar o $geoIntersects, mas não consegui implementar a tempo
 - vi que os valores dos documentos não estão padronizados, daria para padronizar ao inserir na base