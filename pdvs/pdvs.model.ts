import * as mongoose from 'mongoose'

export interface Pdv extends mongoose.Document {
  id: number,
  tradingName: string,
  ownerName: string,
  document: string
}

const pdvSchema = new mongoose.Schema({
    id: {
        type: Number,
        unique: true
    },
    tradingName: {
        type: String,
        required: true
    },
    ownerName: {
        type: String,
        required: true
    },
    document: {
        type: String,
        // minlength: 18,
        // maxlength: 18,
        unique: true,
        required: true
    },
    coverageArea: {
        type: {
            type: String
        },
        coordinates: [],
        select: false
    },
    address: {
        type: {
            type: String
        },
        coordinates: {
            // require: true,
            type: [Number]
        },
        select: false
    }
})

pdvSchema.index({ address: '2dsphere' })
export const Pdv = mongoose.model<Pdv>('Pdv', pdvSchema)
