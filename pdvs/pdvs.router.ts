import * as restify from 'restify'
import {Router} from '../common/router'
import {Pdv} from './pdvs.model'
import * as obj from '../data/pdvs.json'

class PdvRouter extends Router {
    applyRoutes(application: restify.Server){

        application.get('/pdvs', (_req, resp, next)=>{
            Pdv.find().then(pdvs=>{
                resp.json(pdvs)
                return next()
            })
        })

        application.get('/pdvs/:lat/:lng', (req, resp, next)=>{
            var distance = 1000
            let lat = req.params.lat
            let lng = req.params.lng

            var query = Pdv.find({
                address: {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [ lat, lng ]
                        },
                        $maxDistance: distance
                    }
                }
            })

            query.exec(function (err, pdv) {
                if (err) {
                    console.log(err)
                    throw err
                }

                if (!pdv) {
                    resp.json({})
                } else {
                    console.log('Pdv nao encontrado')
                    resp.json(pdv)
                }
            })

            return next()

        })

        application.get('/pdvs/:id', (req, resp, next)=>{
            Pdv.find({id: req.params.id}).then(pdv=>{
                if(pdv && pdv.length > 0) {
                    resp.json(pdv)
                    return next()
                }
                resp.send(404)
                return next()
            })
        })

        application.post('/pdvs', (req, resp, next)=>{
            let pdv = new Pdv(req.body)
            pdv.save().then(user=>{
                resp.json(user)
                return next()
            })
        })

        application.post('/pdvs/seed', (_req, resp, next)=>{
            // var obj = require( '../data/pdvs.json' )
            Pdv.collection.deleteMany({})

            for( var i = 0; i < obj.pdvs.length; i++ ) {
                new Pdv( obj.pdvs[ i ] ).save();
        	}

            resp.send(200)
            return next()
        })

    }
}

export const pdvsRouter = new PdvRouter()
