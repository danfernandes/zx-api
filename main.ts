import {Server} from './server/server'
import {pdvsRouter} from './pdvs/pdvs.router'

const server = new Server();

server.bootstrap([pdvsRouter]).then(server=>{
    console.log('Server is listining on: ', server.application.address());
}).catch(error=>{
    console.log('Server failed to start')
    console.log(error)
    process.exit(1)
})
